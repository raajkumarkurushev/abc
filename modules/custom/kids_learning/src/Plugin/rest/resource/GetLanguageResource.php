<?php

namespace Drupal\kids_learning\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "get_languages",
 *   label = @Translation("Get Languages"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/get-languages",
 *   }
 * )
 */
class GetLanguageResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    /* if (!\Drupal::currentUser()->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    } */
    
    $data = [];
    
    try {
      $langcodes = \Drupal::languageManager()->getLanguages();
      
      foreach ($langcodes as $language) {
        $languageId = $language->getId();
        $languageName = $language->getName();
        
        $data[] = [
          'languagecode' => $languageId,
          'languagename' => $languageName,
        ];
      }
    }
    catch(Exception $e) {
      
    }
    
    
    $response = new ResourceResponse($data);
    // In order to generate fresh result every time (without clearing 
    // the cache), you need to invalidate the cache.
    $response->addCacheableDependency($data);
    return $response;
  }

}