<?php

namespace Drupal\kids_learning\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\views\Views;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "get_details",
 *   label = @Translation("Get Details"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/get-categories/{tid}",
 *   }
 * )
 */
class GetDetailsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($tid) {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    /* if (!\Drupal::currentUser()->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    } */
    
    $data = [];
    
    try {
      $currentLanguage = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $arguments = [];
      $arguments[] = $currentLanguage;
      $arguments[] = $tid;
      
      $parentData = $this->getParent([$tid]);
      foreach ($parentData as $key => $value) {
        $data['parentData'][$key] = (array) $value;
      }
      
      $result = $this->getCategoryList($arguments, $tid);
      
      foreach ($result as $key => $value) {
        $data['data'][$key] = (array) $value;
      }
      
    }
    catch(Exception $e) {
      
    }
    
    $response = new ModifiedResourceResponse($data);
    // In order to generate fresh result every time (without clearing 
    // the cache), you need to invalidate the cache.
    //$response->addCacheableDependency($data);
    return $response;
  }
  
  function getParent($arguments) {
    $view = Views::getView('categories');
      
    if (is_object($view)) {
      $view->setDisplay('parent');
      $view->setArguments($arguments);
      $view->execute();
      $result = \Drupal::service('renderer')->render($view->render());
      $result = json_decode($result);
      return $result;
    }
    
    return NULL;
  }
  
  function getCategoryList($arguments, $tid) {
    $view = Views::getView('categories');
      
    if (is_object($view)) {
      $view->setDisplay('rest_export_1');
      $view->setArguments($arguments);
      $view->execute();
      $result = \Drupal::service('renderer')->render($view->render());
      $result = json_decode($result);
      
      $finalResults = [];
	  $weightRecords = $this->getMatchWeight($tid);
	  foreach($weightRecords as $weightRecord) {
		$key = array_search($weightRecord, array_column($result, 'tid_weight'));
		$finalResults[] = $result[$key];
	  }
	  
	  return $finalResults;
    }
    
    return NULL;
  }
  
  function getMatchWeight($tid) {
    $view = Views::getView('admin_content_categories');
    $currentLanguage = \Drupal::languageManager()->getCurrentLanguage()->getId();
      
	$values = [];
    
    if (is_object($view)) {
      $view->setDisplay('page_2');      
      // $view->setExposedInput(['vid' => 'category']);
      $view->setArguments([$tid]);
	  $view->execute();
      $result = $view->result;
      
	  foreach($result as $record) {
        $values[$record->tid] = $record->tid;
      }
    }
    
    return $values;
  }
    
}