<?php

namespace Drupal\kids_learning\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\views\Views;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "match_following",
 *   label = @Translation("Match Followings Details"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/matches/{id}",
 *   }
 * )
 */
class GetMatchFollowingDetails extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($id) {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    /* if (!\Drupal::currentUser()->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    } */
    
    $data = [];
    
    try {
      $currentLanguage = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $arguments = [];
      $arguments[] = $currentLanguage;
      $arguments[] = $id;
      
      $result = $this->getMatchFollowings($arguments);
      
      $consValues = [];
      foreach ($result as $key => $values) {
        $values = (array) $values;
        
        foreach($values as $fieldName => $fieldValue) {
          $jsonFields = ['left', 'right'];
          if(in_array($fieldName, $jsonFields)) {
            
            $fieldValue = json_decode($fieldValue);
            
            $sideValues = array_chunk($fieldValue, 5, true);
            
            foreach($sideValues as $key => $sideValue) {
              shuffle($sideValue);
              
              $tempValues = [];
              foreach($sideValue as $i => $val) {              
                $tempValues[$i] = (array) $val;
              }
              
              $consValues[$key]['name'] = $values['name'];
              $consValues[$key][$fieldName] = $tempValues;
            } 
          }
        }
        
        $data = $consValues;
      }
      
      
    }
    catch(Exception $e) {
      
    }
    
    $response = new ModifiedResourceResponse($data);
    // In order to generate fresh result every time (without clearing 
    // the cache), you need to invalidate the cache.
    //$response->addCacheableDependency($data);
    return $response;
  }
  
  function getMatchFollowings($arguments) {
    $view = Views::getView('match_the_following');
      
    if (is_object($view)) {
      $view->setDisplay('rest_export_1');
      $view->setArguments($arguments);
      $view->execute();
      $result = \Drupal::service('renderer')->render($view->render());
      $result = json_decode($result);
      return $result;
    }
    
    return NULL;
  }
  
}