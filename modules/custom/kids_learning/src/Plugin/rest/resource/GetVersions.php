<?php

namespace Drupal\kids_learning\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\views\Views;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "get_versions",
 *   label = @Translation("Get Versions"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/get-versions",
 *   }
 * )
 */
class GetVersions extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    $data = [];
     
    try {
      $version_number = \Drupal::config('kids_learning.settings')->get('version_number');
      
      $data = ['version_number' => $version_number];
    }
    catch(Exception $e) {
      
    }
    
    $response = new ModifiedResourceResponse($data);
    // In order to generate fresh result every time (without clearing 
    // the cache), you need to invalidate the cache.
    //$response->addCacheableDependency($data);
    return $response;
  }
  
  
}