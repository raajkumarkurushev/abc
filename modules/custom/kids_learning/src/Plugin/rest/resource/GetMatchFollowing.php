<?php

namespace Drupal\kids_learning\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\views\Views;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "match_following_category",
 *   label = @Translation("Match Followings Category"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/matches",
 *   }
 * )
 */
class GetMatchFollowing extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    /* if (!\Drupal::currentUser()->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    } */
    
    $data = [];
    
    try {
      $currentLanguage = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $arguments = [];
      $arguments[] = $currentLanguage;
      
      $result = $this->getMatchFollowings($arguments);
      
      foreach ($result as $key => $values) {
        $values = (array) $values;
        
        $data[$key] = $values;
      }
      
    }
    catch(Exception $e) {
      
    }
    
    $response = new ModifiedResourceResponse($data);
    // In order to generate fresh result every time (without clearing 
    // the cache), you need to invalidate the cache.
    //$response->addCacheableDependency($data);
    return $response;
  }
  
  function getMatchFollowings($arguments) {
    $view = Views::getView('match_the_following');
      
    if (is_object($view)) {
      $view->setDisplay('rest_export_2');
      $view->setArguments($arguments);
      $view->execute();
      $result = \Drupal::service('renderer')->render($view->render());
      $result = json_decode($result);
	  
	  $finalResults = [];
	  $weightRecords = $this->getMatchWeight($arguments);
	  foreach($weightRecords as $weightRecord) {
		$key = array_search($weightRecord, array_column($result, 'tid_weight'));
		$finalResults[] = $result[$key];
	  }
	  
	  return $finalResults;
    }
    
    return NULL;
  }
   
  function getMatchWeight($arguments) {
    $view = Views::getView('admin_content_categories');
    $values = [];
    
    if (is_object($view)) {
      $view->setDisplay('page_1');      
      $view->setExposedInput(['vid' => 'match_the_following']);
      $view->execute();
      $result = $view->result;
      
	  foreach($result as $record) {
        $values[$record->tid] = $record->tid;
      }
    }
    
    return $values;
  }
  
}