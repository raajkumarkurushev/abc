<?php

namespace Drupal\kids_learning\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\views\Views;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "fill_blank",
 *   label = @Translation("Fill Blank"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fill-blanks",
 *   }
 * )
 */
class GetFillBlank extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    /* if (!\Drupal::currentUser()->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    } */
    
    $data = [];
    
    try {
      $currentLanguage = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $arguments = [];
      $arguments[] = $currentLanguage;
      
      $result = $this->getFillBlanks($arguments);
      
      foreach ($result as $key => $values) {
        $values = (array) $values;
        
        foreach($values as $fieldName => $fieldValue) {
          $jsonFields = ['field_word', 'field_relevant_letters'];
          
          if(in_array($fieldName, $jsonFields)) {
            $fieldValue = json_decode($fieldValue);
            $fieldValue = (array) $fieldValue;
            
            $tempField = [];
            $tempField['position'] = $fieldValue['position'];
            unset($fieldValue['position']);
            $letters = array_filter($fieldValue);
            $letters = (array) $letters;
            
            $tempLetters = [];
            foreach($letters as $key => $val) {
              $tempLetters[] = ['letter' => $val];
            }
            $tempField['letters'] = $tempLetters;
            
            $fieldValue = $tempField;
          }
          
          $values[$fieldName] = $fieldValue;
        }
        
        $data[] = $values;
      }
      
      
    }
    catch(Exception $e) {
      
    }
    $data = (array) $data;
    $response = new ModifiedResourceResponse($data);
    // In order to generate fresh result every time (without clearing 
    // the cache), you need to invalidate the cache.
    //$response->addCacheableDependency($data);
    return $response;
  }
  
  function getFillBlanks($arguments) {
    $view = Views::getView('fill_in_the_blanks');
      
    if (is_object($view)) {
      $view->setDisplay('rest_export_1');
      $view->setArguments($arguments);
      $view->execute();
      $result = \Drupal::service('renderer')->render($view->render());
      $result = json_decode($result);
      
	  $finalResults = [];
	  $weightRecords = $this->getMatchWeight($arguments);
	  foreach($weightRecords as $weightRecord) {
		$key = array_search($weightRecord, array_column($result, 'tid_weight'));
		$finalResults[] = $result[$key];
	  }
	  
	  return $finalResults;
    }
    
    return NULL;
  }
  
  function getMatchWeight($arguments) {
    $view = Views::getView('admin_content_categories');
    $values = [];
    
    if (is_object($view)) {
      $view->setDisplay('page_1');      
      $view->setExposedInput(['vid' => 'fill_blank']);
      $view->execute();
      $result = $view->result;
      
	  foreach($result as $record) {
        $values[$record->tid] = $record->tid;
      }
    }
    
    return $values;
  }
  
}