<?php

namespace Drupal\kids_learning\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'Word' field type.
 *
 * @FieldType(
 *   id = "Word",
 *   label = @Translation("Word"),
 *   description = @Translation("Stores an Word."),
 *   category = @Translation("Custom"),
 *   default_widget = "WordDefaultWidget",
 *   default_formatter = "WordDefaultFormatter"
 * )
 */
class Word extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
	$newValues = [];
	if(isset($values['container'])) {
		$words = $values['container']['word'];
		$position = $values['container']['position'];
		
		$newValues = $words;
		$newValues['position'] = $position;
	}
	else {
		$newValues = $values;
	}
	
    parent::setValue($newValues, $notify);
  }

  /**
   * Field type properties definition.
   * 
   * Inside this method we defines all the fields (properties) that our 
   * custom field type will have.
   * 
   * Here there is a list of allowed property types: https://goo.gl/sIBBgO
   */
  public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

    $properties['letter_01'] = DataDefinition::create('string')
      ->setLabel(t('letter_01'));

    $properties['letter_02'] = DataDefinition::create('string')
      ->setLabel(t('letter_02'));

    $properties['letter_03'] = DataDefinition::create('string')
      ->setLabel(t('letter_03'));

    $properties['letter_04'] = DataDefinition::create('string')
      ->setLabel(t('letter_04'));

    $properties['letter_05'] = DataDefinition::create('string')
      ->setLabel(t('letter_05'));

    $properties['letter_06'] = DataDefinition::create('string')
      ->setLabel(t('letter_06'));

    $properties['letter_07'] = DataDefinition::create('string')
      ->setLabel(t('letter_07'));

    $properties['letter_08'] = DataDefinition::create('string')
      ->setLabel(t('letter_08'));

    $properties['letter_09'] = DataDefinition::create('string')
      ->setLabel(t('letter_09'));

    $properties['letter_10'] = DataDefinition::create('string')
      ->setLabel(t('letter_10'));

    $properties['letter_11'] = DataDefinition::create('string')
      ->setLabel(t('letter_11'));

    $properties['letter_12'] = DataDefinition::create('string')
      ->setLabel(t('letter_12'));

    $properties['position'] = DataDefinition::create('string')
      ->setLabel(t('position'));

    return $properties;
  }

  /**
   * Field type schema definition.
   * 
   * Inside this method we defines the database schema used to store data for 
   * our field type.
   * 
   * Here there is a list of allowed column types: https://goo.gl/YY3G7s
   */
  public static function schema(StorageDefinition $storage) {

    $columns = [];
    $columns['letter_01'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_02'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_03'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_04'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_05'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_06'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_07'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_08'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_09'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_10'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_11'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['letter_12'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    $columns['position'] = [
      'type' => 'char',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ];
    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * Define when the field type is empty. 
   * 
   * This method is important and used internally by Drupal. Take a moment
   * to define when the field fype must be considered empty.
   */
  public function isEmpty() {
	$isEmpty = 
      empty($this->get('letter_01')->getValue()) &&
      empty($this->get('letter_02')->getValue()) &&
      empty($this->get('position')->getValue());

    return false;
  }

} // class