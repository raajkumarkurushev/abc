<?php

namespace Drupal\kids_learning\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;

/**
 * Plugin implementation of the 'WordDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "WordDefaultFormatter",
 *   label = @Translation("Word"),
 *   field_types = {
 *     "Word"
 *   }
 * )
 */
class WordDefaultFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   * 
   * Inside this method we can customize how the field is displayed inside 
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
	  $value = $item->getValue();
      $item = (array) $value;
	  
	  $output = implode(', ', $item);
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $output,
      ];
    }

    return $elements;
  }
  
} // class