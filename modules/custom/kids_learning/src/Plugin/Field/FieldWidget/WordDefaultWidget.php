<?php

namespace Drupal\kids_learning\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'WordDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "WordDefaultWidget",
 *   label = @Translation("Word select"),
 *   field_types = {
 *     "Word"
 *   }
 * )
 */
class WordDefaultWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   * 
   * Inside this method we can define the form used to edit the field type.
   * 
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta, 
    Array $element, 
    Array &$form, 
    FormStateInterface $formState
  ) {
	  
    $element['container'] = [
      '#type' => 'fieldset',
      '#title' => $element['#title'],
    ];
	
    $element['container']['word'] = [
      '#type' => 'fieldset',
      '#title' => t('Word'),
      '#attributes' => ['class' => ['word-wrapper']],
    ];
	
    $element['container']['word']['letter_01'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 01'),
      '#default_value' => isset($items[$delta]->letter_01) ? $items[$delta]->letter_01 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_02'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 02'),
      '#default_value' => isset($items[$delta]->letter_02) ? $items[$delta]->letter_02 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_03'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 03'),
      '#default_value' => isset($items[$delta]->letter_03) ? $items[$delta]->letter_03 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_04'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 04'),
      '#default_value' => isset($items[$delta]->letter_04) ? $items[$delta]->letter_04 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_05'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 05'),
      '#default_value' => isset($items[$delta]->letter_05) ? $items[$delta]->letter_05 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_06'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 06'),
      '#default_value' => isset($items[$delta]->letter_06) ? $items[$delta]->letter_06 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_07'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 07'),
      '#default_value' => isset($items[$delta]->letter_07) ? $items[$delta]->letter_07 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_08'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 08'),
      '#default_value' => isset($items[$delta]->letter_08) ? $items[$delta]->letter_08 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_09'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 09'),
      '#default_value' => isset($items[$delta]->letter_09) ? $items[$delta]->letter_09 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_10'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 10'),
      '#default_value' => isset($items[$delta]->letter_10) ? $items[$delta]->letter_10 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_11'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 11'),
      '#default_value' => isset($items[$delta]->letter_11) ? $items[$delta]->letter_11 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['word']['letter_12'] = [
      '#type' => 'textfield',
      '#title' => t('Letter 12'),
      '#default_value' => isset($items[$delta]->letter_12) ? $items[$delta]->letter_12 : null,
      '#required' => FALSE,
      '#title_display' => 'none',
      //'#placeholder' => t('Street'),
    ];
	
    $element['container']['position'] = [
      '#type' => 'number',
      '#title' => t('Position'),
      '#default_value' => isset($items[$delta]->position) ? $items[$delta]->position : null,
      '#required' => FALSE,
      //'#placeholder' => t('Street'),
    ];
    
    $element['#attached']['library'][] = 'kids_learning/word';
    
    return $element;
  }

} // class